import config from 'config';
import dev from '../devConfig.mjs';
const mode = config.get('mode');

const getCredentials = () => {
    if (mode === 'dev') {
        return dev;
    } else if (mode === 'prod') {
        return 'prod config';
    }
}

export default getCredentials();