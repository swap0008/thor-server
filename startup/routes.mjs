import express from 'express';
import cors from 'cors';
import company from '../routes/company/routes.mjs';
import employee from '../routes/employee/routes.mjs';
import itinerary from '../routes/itinerary/routes.mjs';
import customer from '../routes/customer/routes.mjs';
import proposal from '../routes/proposal/routes.mjs';
import event from '../routes/event/routes.mjs';
import day from '../routes/day/routes.mjs';
import googleOAuth from '../routes/google_oauth/routes.mjs';
import contact from '../routes/contact/routes.mjs';
import calendar from '../routes/calendar/routes.mjs';

export default function(app) {
    app.use(cors({ exposedHeaders: 'x-auth-token' }));
    app.use(express.json());
    app.use('/employee', employee);
    app.use('/company', company);
    app.use('/itinerary', itinerary);
    app.use('/customer', customer);
    app.use('/proposal', proposal);
    app.use('/event', event);
    app.use('/day', day);
    app.use('/google_oauth', googleOAuth);
    app.use('/contact', contact);
    app.use('/calendar', calendar);
    app.use('/push-notification', (req, res) => {
        console.log(req.body);
        console.log(req.query);
        res.send(req.body);
    })
}