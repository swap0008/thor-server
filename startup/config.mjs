import config from 'config';

export default function () {
    if (!config.get('jwtPrivateKey')) {
        console.error('Fatal Error: jwtPrivateKey is not defined.');
        process.exit(1);
    }
}