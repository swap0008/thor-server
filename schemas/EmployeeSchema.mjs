import Joi from '@hapi/joi';
import EmployeeModel from '../models/EmployeeModel.mjs';

const getFields = () => {
	return {
		[EmployeeModel.NAME]: Joi.string().pattern(/^[a-zA-Z ]+$/),
		[EmployeeModel.EMAIL]: Joi.string().email(),
		[EmployeeModel.COMPANY_ID]: Joi.string().email()
	};
};

export const saveSchema = (data) => {
    const fields = getFields();
	const schema = Joi.object({
		[EmployeeModel.NAME]: fields[EmployeeModel.NAME].required(),
		[EmployeeModel.EMAIL]: fields[EmployeeModel.EMAIL].required(),
        [EmployeeModel.COMPANY_ID]: fields[EmployeeModel.COMPANY_ID].required()
	});

	return schema.validate(data);
};

export const updateSchema = (data) => {
    const fields = getFields();
	const schema = Joi.object({
		[EmployeeModel.NAME]: fields[EmployeeModel.NAME],
		[EmployeeModel.EMAIL]: fields[EmployeeModel.EMAIL],
		[EmployeeModel.COMPANY_ID]: fields[EmployeeModel.COMPANY_ID]
	});

	return schema.validate(data);
};

export default { saveSchema, updateSchema };
