import Joi from '@hapi/joi';
import ProposalModel from '../models/ProposalModel.mjs';

const getFields = () => {
	return {
		[ProposalModel.NAME]: Joi.string(),
		[ProposalModel.CLIENT]: Joi.string().pattern(/^[a-zA-Z ]+$/),
		[ProposalModel.DURATION]: Joi.string().pattern(/[0-9]D\/[0-9]N/),
		[ProposalModel.DATES]: Joi.string().pattern(/^[0-9-0-9]+$/),
		[ProposalModel.STATUS]: Joi.string().pattern(/query|paid/),
		[ProposalModel.COST]: Joi.number(),
		[ProposalModel.COMPANY_ID]: Joi.string().email(),
		[ProposalModel.EMPLOYEE_ID]: Joi.string().email()
	};
};

export const saveSchema = (data) => {
	const fields = getFields();
	const schema = Joi.object({
		[ProposalModel.NAME]: fields[ProposalModel.NAME].required(),
		[ProposalModel.CLIENT]: fields[ProposalModel.CLIENT].required(),
		[ProposalModel.COST]: fields[ProposalModel.COST].required(),
		[ProposalModel.DURATION]: fields[ProposalModel.DURATION].required(),
		[ProposalModel.DATES]: fields[ProposalModel.DATES].required(),
		[ProposalModel.STATUS]: fields[ProposalModel.STATUS].required(),
		[ProposalModel.COMPANY_ID]: fields[ProposalModel.COMPANY_ID].required(),
		[ProposalModel.EMPLOYEE_ID]: fields[ProposalModel.EMPLOYEE_ID].required()
	});

	return schema.validate(data);
};

export const updateSchema = (data) => {
	const fields = getFields();
	const schema = Joi.object({
		[ProposalModel.NAME]: fields[ProposalModel.NAME],
		[ProposalModel.CLIENT]: fields[ProposalModel.CLIENT],
		[ProposalModel.COST]: fields[ProposalModel.COST],
		[ProposalModel.DURATION]: fields[ProposalModel.DURATION],
		[ProposalModel.DATES]: fields[ProposalModel.DATES],
		[ProposalModel.STATUS]: fields[ProposalModel.STATUS],
		[ProposalModel.COMPANY_ID]: fields[ProposalModel.COMPANY_ID],
		[ProposalModel.EMPLOYEE_ID]: fields[ProposalModel.EMPLOYEE_ID]
	});

	return schema.validate(data);
};

export default { saveSchema, updateSchema };
