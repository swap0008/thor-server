import Joi from '@hapi/joi';
import CustomerModel from '../models/CustomerModel.mjs';

const getFields = () => {
	return {
		[CustomerModel.NAME]: Joi.string().pattern(/^[a-zA-Z ]+$/),
		[CustomerModel.EMAIL]: Joi.string().email(),
		[CustomerModel.PHONE]: Joi.number().integer(),
		[CustomerModel.COUNTRY]: Joi.string(),
		[CustomerModel.STATE]: Joi.string(),
		[CustomerModel.CITY]: Joi.string()
	};
};

export const saveSchema = (data) => {
    const fields = getFields();
	const schema = Joi.object({
		[CustomerModel.NAME]: fields[CustomerModel.NAME].required(),
		[CustomerModel.EMAIL]: fields[CustomerModel.EMAIL].required(),
		[CustomerModel.PHONE]: fields[CustomerModel.PHONE].required(),
		[CustomerModel.COUNTRY]: fields[CustomerModel.COUNTRY].required(),
		[CustomerModel.STATE]: fields[CustomerModel.STATE].required(),
		[CustomerModel.CITY]: fields[CustomerModel.CITY].required()
	});

	return schema.validate(data);
};

export const updateSchema = (data) => {
	const fields = getFields();
	const schema = Joi.object({
		[CustomerModel.NAME]: fields[CustomerModel.NAME],
		[CustomerModel.EMAIL]: fields[CustomerModel.EMAIL],
		[CustomerModel.PHONE]: fields[CustomerModel.PHONE],
		[CustomerModel.COUNTRY]: fields[CustomerModel.COUNTRY],
		[CustomerModel.STATE]: fields[CustomerModel.STATE],
		[CustomerModel.CITY]: fields[CustomerModel.CITY]
	});

	return schema.validate(data);
};

export default { saveSchema, updateSchema };
