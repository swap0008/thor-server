import Joi from '@hapi/joi';
import ItineraryModel from '../models/ItineraryModel.mjs';

const getFields = () => {
	return {
		[ItineraryModel.NAME]: Joi.string(),
		[ItineraryModel.SMALL_DESCRIPTION]: Joi.string(),
		[ItineraryModel.LANGUAGE]: Joi.string().pattern(/^[a-z]+$/).min(2).max(2),
		[ItineraryModel.CURRENCY]: Joi.string().pattern(/^[a-z]+$/).min(3).max(3),
		[ItineraryModel.COST]: Joi.number(),
		[ItineraryModel.COMPANY_ID]: Joi.string().email(),
		[ItineraryModel.EMPLOYEE_ID]: Joi.string().email()
	};
};

export const saveSchema = (data) => {
	const fields = getFields();
	const schema = Joi.object({
		[ItineraryModel.NAME]: fields[ItineraryModel.NAME].required(),
		[ItineraryModel.SMALL_DESCRIPTION]: fields[ItineraryModel.SMALL_DESCRIPTION].required(),
		[ItineraryModel.LANGUAGE]: fields[ItineraryModel.LANGUAGE].required(),
		[ItineraryModel.CURRENCY]: fields[ItineraryModel.CURRENCY].required(),
		[ItineraryModel.COST]: fields[ItineraryModel.COST].required(),
		[ItineraryModel.COMPANY_ID]: fields[ItineraryModel.COMPANY_ID].required(),
		[ItineraryModel.EMPLOYEE_ID]: fields[ItineraryModel.EMPLOYEE_ID].required()
	});

	return schema.validate(data);
};

export const updateSchema = (data) => {
	const fields = getFields();
	const schema = Joi.object({
        [ItineraryModel.NAME]: fields[ItineraryModel.NAME],
        [ItineraryModel.SMALL_DESCRIPTION]: fields[ItineraryModel.SMALL_DESCRIPTION],
        [ItineraryModel.LANGUAGE]: fields[ItineraryModel.LANGUAGE],
        [ItineraryModel.CURRENCY]: fields[ItineraryModel.CURRENCY],
        [ItineraryModel.COST]: fields[ItineraryModel.COST],
        [ItineraryModel.COMPANY_ID]: fields[ItineraryModel.COMPANY_ID],
        [ItineraryModel.EMPLOYEE_ID]: fields[ItineraryModel.EMPLOYEE_ID]
    });

	return schema.validate(data);
};

export default { saveSchema, updateSchema };
