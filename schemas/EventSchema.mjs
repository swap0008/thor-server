import Joi from '@hapi/joi';
import EventModel from '../models/EventModel.mjs';

const getFields = () => {
	return {
		[EventModel.NAME]: Joi.string().pattern(/^[a-zA-Z ]+$/),
		[EventModel.TYPE]: Joi.string(),
		[EventModel.COLUMN]: Joi.number(),
		[EventModel.POSITION]: Joi.number(),
		[EventModel.DAY_ID]: Joi.string(),
		[EventModel.ROW]: Joi.number(),
		[EventModel.ID]: Joi.string()
	};
};

export const saveSchema = (data) => {
	const fields = getFields();
	const schema = Joi.object({
		[EventModel.NAME]: fields[EventModel.NAME].required(),
		[EventModel.TYPE]: fields[EventModel.TYPE].required(),
		[EventModel.COLUMN]: fields[EventModel.COLUMN].required(),
		[EventModel.POSITION]: fields[EventModel.POSITION],
		[EventModel.DAY_ID]: fields[EventModel.DAY_ID].required(),
		[EventModel.ROW]: fields[EventModel.ROW].required()
	});

	return schema.validate(data);
};

export const updateSchema = (data) => {
	const fields = getFields();
	const schema = Joi.object({
		[EventModel.NAME]: fields[EventModel.NAME],
		[EventModel.TYPE]: fields[EventModel.TYPE],
		[EventModel.POSITION]: fields[EventModel.POSITION]
	});

	return schema.validate(data);
};

export const deleteSchema = (data) => {
	const fields = getFields();
	const schema = Joi.object({
		[EventModel.ID]: fields[EventModel.ID].required(),
		[EventModel.COLUMN]: fields[EventModel.COLUMN].required(),
		[EventModel.POSITION]: fields[EventModel.POSITION].required(),
		[EventModel.DAY_ID]: fields[EventModel.DAY_ID].required(),
		[EventModel.ROW]: fields[EventModel.ROW].required()
	});

	return schema.validate(data);
}

export default { saveSchema, updateSchema, deleteSchema };
