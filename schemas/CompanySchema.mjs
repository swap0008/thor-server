import Joi from '@hapi/joi';
import CompanyModel from '../models/CompanyModel.mjs';

const getFields = () => {
	return {
		[CompanyModel.NAME]: Joi.string().pattern(/^[a-zA-Z ]+$/),
		[CompanyModel.EMAIL]: Joi.string().email(),
		[CompanyModel.PHONE]: Joi.number().integer(),
		[CompanyModel.COUNTRY]: Joi.string(),
		[CompanyModel.STATE]: Joi.string(),
		[CompanyModel.CITY]: Joi.string(),
		[CompanyModel.LANGUAGES]: Joi.array().unique(),
		[CompanyModel.PRIMARY_DESTINATIONS]: Joi.array().unique()
	};
};

export const saveSchema = (data) => {
    const fields = getFields();
	const schema = Joi.object({
		[CompanyModel.NAME]: fields[CompanyModel.NAME].required(),
		[CompanyModel.EMAIL]: fields[CompanyModel.EMAIL].required(),
		[CompanyModel.PHONE]: fields[CompanyModel.PHONE].required(),
		[CompanyModel.COUNTRY]: fields[CompanyModel.COUNTRY].required(),
		[CompanyModel.STATE]: fields[CompanyModel.STATE].required(),
		[CompanyModel.CITY]: fields[CompanyModel.CITY].required(),
		[CompanyModel.LANGUAGES]: fields[CompanyModel.LANGUAGES].required(),
		[CompanyModel.PRIMARY_DESTINATIONS]: fields[CompanyModel.PRIMARY_DESTINATIONS].required()
	});

	return schema.validate(data);
};

export const updateSchema = (data) => {
	const fields = getFields();
	const schema = Joi.object({
		[CompanyModel.NAME]: fields[CompanyModel.NAME],
		[CompanyModel.EMAIL]: fields[CompanyModel.EMAIL],
		[CompanyModel.PHONE]: fields[CompanyModel.PHONE],
		[CompanyModel.COUNTRY]: fields[CompanyModel.COUNTRY],
		[CompanyModel.STATE]: fields[CompanyModel.STATE],
		[CompanyModel.CITY]: fields[CompanyModel.CITY],
		[CompanyModel.LANGUAGES]: fields[CompanyModel.LANGUAGES],
		[CompanyModel.PRIMARY_DESTINATIONS]: fields[CompanyModel.PRIMARY_DESTINATIONS]
	});

	return schema.validate(data);
};

export default { saveSchema, updateSchema };
