const admin = require('firebase-admin');
const serviceAccount = require('./ServiceAccountKey.json');

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount)
});

const db = admin.firestore();

async function setData() {
    const dbRef = db.collection('company').doc('starkindustry');

    try {
        const result = await dbRef.set({
            name: 'Stark Industry',
            city: 'New York'
        })

        console.log(result);
    } catch (err) {
        console.log(err);
    }
}

async function getData() {
    const dbRef = db.collection('company').doc('starkindustry');
    try {
        const result = await dbRef.get();
        console.log(result.id, result.data());
    } catch(err) {
        console.log(err);
    }
}

getData();