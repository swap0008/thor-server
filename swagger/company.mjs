export default {
    paths: {
        '/company/all': {
            get: {
                description: 'Get companies',
                operationId: 'getCompanies'
            },
            parameters: [],
            responses: {
                '200': {
                    description: 'All companies data fetched'
                },
                '400': {
                    description: 'Bad Request'
                }
            }
        },
        '/company': {
            post: {
                description: 'Save company',
                operationId: 'saveCompany'
            },
            responses: {
                '200': {
                    description: 'Company information saved.'
                }
            }
        }
    }
}