import company from './company.mjs';

export default {
    openapi: '3.0.1',
    info: {
        title: 'Thor Server API',
        description: 'Thor server API information',
        contact: {
            name: 'Swapnil Sharma',
            email: 'swapnil@envisionlabs.io'
        }
    },
    servers: [
        {
            url: 'http://localhost:4000',
            description: 'Local server'
        }
    ],
    ...company
}