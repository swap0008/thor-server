import express from 'express';

const router = express.Router();

import {
    startOAuth,
    finishOAuth
} from './controllers.mjs';

router.post('/start', startOAuth);
router.get('/stop', finishOAuth);


export default router;