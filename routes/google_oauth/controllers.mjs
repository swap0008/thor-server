import GoogleApis from 'googleapis';
import credentials from './credentials.mjs';
import CompanyModel from '../../models/CompanyModel.mjs';
import EmployeeModel from '../../models/EmployeeModel.mjs';
import OAuth2Model from '../../models/OAuth2Model.mjs';

const Company = new CompanyModel();
const Employee = new EmployeeModel();
const OAuth2 = new OAuth2Model();

const { google } = GoogleApis;
const { client_secret, client_id, redirect_uris } = credentials.web;
const oAuth2Client = new google.auth.OAuth2(client_id, client_secret, redirect_uris[1] + '/google_oauth/stop');

const defaultScopes = [
    'https://www.googleapis.com/auth/userinfo.email'
]

export const startOAuth = async (req, res) => {
    const companyId = req.body[OAuth2Model.COMPANY_ID];
    const employeeId = req.body[OAuth2Model.EMPLOYEE_ID];

    const { scopes = [] } = req.body;

    let state = {};
    if (companyId && employeeId) {
        state = { companyId, employeeId };

        const [company, employee] = await Promise.all([Company.getDoc(companyId), Employee.getDoc(employeeId)]);

        if (!company.exists || !employee.exists) return res.status(400).send('Company or Employee does not exists!');
    } else {
        state = { companyId };

        const company = await Company.getDoc(companyId);
        if (!company.exists) return res.status(400).send('Company does not exists!');
    }

    state = JSON.stringify({ ...state, scopes });

    const authUrl = oAuth2Client.generateAuthUrl({
        access_type: 'offline',
        scope: defaultScopes.concat(scopes.map(scope => `https://www.googleapis.com/auth/${scope}`)),
        state
    });

    console.log(authUrl);
    res.redirect(authUrl);
}

export const finishOAuth = async (req, res) => {
    const { tokens } = await oAuth2Client.getToken(req.query.code);
    const { companyId, employeeId, scopes } = JSON.parse(req.query.state);

    oAuth2Client.setCredentials(tokens);

    const service = google.oauth2({ version: 'v1', auth: oAuth2Client });

    const { data: { email } } = await service.userinfo.get();

    let query = OAuth2.collectionRef.where(OAuth2Model.COMPANY_ID, '==', companyId);
    if (companyId && employeeId) query = query.where(OAuth2Model.EMPLOYEE_ID, '==', employeeId);
    else query = query.where(OAuth2Model.EMPLOYEE_ID,'==', 'company');

    const snapshot = await query.get();

    const secrets = {};
    scopes.forEach(scope => {
        secrets[scope] = { access_token: tokens.access_token, refresh_token: tokens.refresh_token }
    });

    if (snapshot.empty) {
        let body = {
            [OAuth2Model.COMPANY_ID]: companyId,
            [OAuth2Model.GOOGLE_ACCOUNTS]: {
                [email]: secrets
            }
        };
        if (employeeId) body = { ...body, [OAuth2Model.EMPLOYEE_ID]: employeeId };

        const doc = await OAuth2.add(body);
        return res.send(doc.id);
    }

    let { google_accounts, id } = snapshot.docs.map(doc => ({ id: doc.id, ...doc.data() }))[0];
    google_accounts = {
        ...google_accounts,
        [email]: {
            ...google_accounts[email],
            ...secrets
        }
    };

    await OAuth2.update(id, { google_accounts });
    res.send('Google account added!');
}