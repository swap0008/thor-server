import _ from 'lodash';
import GoogleApis from 'googleapis';
import credentials from '../google_oauth/credentials.mjs';
import ContactModel from '../../models/ContactModel.mjs';
import OAuth2Model from '../../models/OAuth2Model.mjs';

const Contact = new ContactModel();
const OAuth2 = new OAuth2Model();

const { google } = GoogleApis;
const { client_secret, client_id, redirect_uris } = credentials.web;

const parseGoogleContacts = (connections) => {
    const contacts = [];
    connections.forEach(({ names, emailAddresses = [], addresses = [], phoneNumbers = [], resourceName, etag }) => {
        const data = {
            [ContactModel.FIRST_NAME]: names[0].givenName || '',
            [ContactModel.LAST_NAME]: names[0].familyName || '',
            [ContactModel.PHONE_NUMBERS]: phoneNumbers.map(phone => phone.value),
            [ContactModel.DEFAULT_PHONE]: phoneNumbers[0] ? phoneNumbers[0].value : '',
            [ContactModel.EMAIL_ADDRESSES]: emailAddresses.map(email => email.value),
            [ContactModel.ADDRESS]: addresses.map(address => address.streetAddress),
            [ContactModel.NOTES]: '',
            [ContactModel.TYPE]: 'google_contacts',
            resourceName,
            etag
        }

        contacts.push(data);
    });

    return contacts;
}

const commonChecks = async (req, res) => {
    const companyId = req.body[OAuth2Model.COMPANY_ID];
    const employeeId = req.body[OAuth2Model.EMPLOYEE_ID];

    let query = OAuth2.collectionRef.where(OAuth2Model.COMPANY_ID, '==', companyId);
    if (companyId && employeeId) query = query.where(OAuth2Model.EMPLOYEE_ID, '==', employeeId);
    else query = query.where(OAuth2Model.EMPLOYEE_ID, '==', 'company');

    const snapshot = await query.get();
    if (snapshot.empty) return res.status(400).send('No matching document.');

    const { google_accounts } = snapshot.docs.map(doc => doc.data())[0];

    const gmails = req.body[OAuth2Model.GOOGLE_ACCOUNTS] || [];

    const services = [];
    gmails.forEach(gmail => {
        if (!google_accounts[gmail]) return;

        const { contacts: { refresh_token } } = google_accounts[gmail];

        const oAuth2Client = new google.auth.OAuth2(client_id, client_secret, redirect_uris[3]);
        oAuth2Client.setCredentials({ refresh_token });

        services.push(google.people({ version: 'v1', auth: oAuth2Client }));
    });

    return services;
}

const updateGoogleContact = async (service, contact) => {
    let { resourceName, etag, first_name, last_name, phone_numbers, email_addresses, default_phone, address } = contact;
    let phoneNumbers = [{ value: default_phone }];
    phone_numbers.forEach(value => {
        if (value !== default_phone) phoneNumbers.push({ value });
    });

    const { data } = await service[0].people.updateContact({
        resourceName,
        updatePersonFields: 'names,addresses,emailAddresses,phoneNumbers',
        requestBody: {
            etag,
            names: [{ familyName: last_name, givenName: first_name }],
            addresses: address.map(address => ({ streetAddress: address })),
            phoneNumbers,
            emailAddresses: email_addresses.map(value => ({ value })),
        }
    });

    return data;
}

const createGoogleContact = async (service, contact) => {
    let { first_name, last_name, phone_numbers, email_addresses, address, default_phone } = contact;
    let phoneNumbers = [{ value: default_phone }];
    phone_numbers.forEach(value => {
        if (value !== default_phone) phoneNumbers.push({ value });
    });

    await service.people.createContact({
        requestBody: {
            names: [{ familyName: last_name, givenName: first_name }],
            addresses: Array.isArray(address) ? address.map(streetAddress => ({ streetAddress })) : [{ streetAddress: address }],
            phoneNumbers,
            emailAddresses: email_addresses.map(value => ({ value })),
        }
    });
}

const getGoogleContacts = async (service) => {
    const { data: { connections } } = await service.people.connections.list({
        resourceName: 'people/me',
        pageSize: 10,
        personFields: 'addresses,emailAddresses,names,phoneNumbers,taglines'
    });

    return parseGoogleContacts(connections)
}

export const getContacts = async (req, res) => {
    const services = await commonChecks(req, res);
    let contacts = [];

    for (const service of services) {
        const contact = await getGoogleContacts(service);
        contacts = contacts.concat(contact);
    }

    res.send(contacts);
}

export const saveContact = async (req, res) => {
    const data = _.pick(req.body, [
        ContactModel.FIRST_NAME,
        ContactModel.LAST_NAME,
        ContactModel.PHONE_NUMBERS,
        ContactModel.EMAIL_ADDRESSES,
        ContactModel.ADDRESS,
        ContactModel.NOTES,
        ContactModel.TYPE,
        ContactModel.COMPANY_ID,
        ContactModel.EMPLOYEE_ID
    ]);

    data[ContactModel.DEFAULT_PHONE] = data[ContactModel.PHONE_NUMBERS][0] || '';

    const { id } = await Contact.collectionRef.doc();

    await Contact.set(id, { ...data, id });
    res.send(id);
}

export const syncContacts = async (req, res) => {
    //Update Single Google Contacts
    const { contact = {} } = req.body;

    if (contact.resourceName && contact.etag) {
        const service = await commonChecks(req, res);

        const data = await updateGoogleContact(service, contact);

        return res.send({ resourceName: data.resourceName, etag: data.etag });
    }

    const { export_from, export_to } = req.body;
    if (export_from && export_to) req.body[OAuth2Model.GOOGLE_ACCOUNTS] = [export_from, export_to];

    const services = await commonChecks(req, res);

    //export all contacts from one google account to another
    if (export_from && export_to) {
        if (!services[0]) return res.send(`${export_from} acccount does not exists!`);
        const contacts = await getGoogleContacts(services[0]);

        if (!services[1]) return res.send(`${export_to} acccount does not exists!`);
        for (const contact of contacts) await createGoogleContact(services[1], contact);

        return res.send(`Contacts exported from ${export_from} to ${export_to}`);
    }

    //export specific contacts from one google account to another accounts
    const { google_contacts } = req.body;

    if (google_contacts) {
        for (const service of services) {
            try {
                for (const contact of google_contacts) {
                    await createGoogleContact(service, contact);
                };
            } catch (e) {
                console.log(e);
            }
        };

        return res.send('Contacts exported!');
    }

    //export specific local contacts to multiple/one google contacts
    const { contact_ids } = req.body;

    if (contact_ids) {
        const snapshot = await Contact.collectionRef.where(ContactModel.ID, 'in', contact_ids).get();
        if (snapshot.empty) return res.status(400).send('No contact found!');

        const contacts = snapshot.docs.map(doc => doc.data());

        for (const service of services) {
            try {
                for (const contact of contacts) {
                    await createGoogleContact(service, contact);
                };
            } catch (e) {
                console.log(e);
            }
        };

        return res.send('Contacts exported!');
    }


    //export all local contacts to mutiple/one google contacts
    const { company_id, employee_id } = req.body;

    let query = Contact.collectionRef.where(ContactModel.COMPANY_ID, '==', company_id);
    if (company_id && employee_id) query = query.where(ContactModel.EMPLOYEE_ID, '==', employee_id);

    const snapshot = await query.get();
    if (snapshot.empty) return res.send('Nothing to sync!');

    const contacts = snapshot.docs.map(doc => doc.data());

    let contactIds = [];
    for (const service of services) {
        try {
            for (const contact of contacts) {
                await createGoogleContact(service, contact);
                contactIds.push(contact.id);
            };
        } catch (e) {
            console.log(e);
        }
    };

    contactIds = [...new Set(contactIds)];
    if (contactIds.length !== 0) await Contact.deleteMultipleDoc(contactIds);
    res.send('Sync Completed!')
} 