import express from 'express';

const router = express.Router();

import {
    getContacts,
    saveContact,
    syncContacts
} from './controllers.mjs';

router.post('/all', getContacts);
router.post('/save', saveContact);
router.patch('/update', syncContacts);

export default router;