import express from 'express';
// import auth from '../../middlewares/auth.mjs';

const router = express.Router();

//Controllers
import { 
   getEmployee,
   getEmployees,
   saveEmployee,
   deleteEmployee,
   updateEmployee,
   contactSyncRequest
} from './controllers.mjs';

router.get('/all', getEmployees);
router.post('/', getEmployee);
router.post('/save', saveEmployee);
router.put('/', updateEmployee);
router.delete('/', deleteEmployee);
router.post('/contacts_sync_requests', contactSyncRequest);

export default router;