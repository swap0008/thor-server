import _ from 'lodash';
import EmployeeModel from '../../models/EmployeeModel.mjs';
import CompanyModel from '../../models/CompanyModel.mjs';
import ContactSyncRequestModel from '../../models/ContactSyncRequestModel.mjs';
import { saveSchema, updateSchema } from '../../schemas/EmployeeSchema.mjs';

const Employee = new EmployeeModel();
const Company = new CompanyModel();
const ContactSyncRequest = new ContactSyncRequestModel();

export const getEmployee = async (req, res) => {
	const { id } = req.body;
	const employee = await Employee.getDoc(id);
	res.status(200).send(employee.data());
}

export const getEmployees = async (req, res) => {
	const Employees = await Employee.get();
	res.status(200).send(Employees);
};

export const saveEmployee = async (req, res) => {
	const data = _.pick(req.body, [EmployeeModel.EMAIL, EmployeeModel.NAME, EmployeeModel.COMPANY_ID]);

	const { error } = saveSchema(data);
	if (error) return res.status(400).send(error.details[0].message);

	const result = await Employee.set(data[EmployeeModel.EMAIL], data);
	res.status(200).send(result);
};

export const updateEmployee = async (req, res) => {
	const data = _.pick(req.body, [EmployeeModel.NAME, EmployeeModel.COMPANY_ID]);

	const { error } = updateSchema(data);
	if (error) return res.status(400).send(error.details[0].message);

	const { id } = req.body;

	const result = await Employee.update(id, data);
	res.status(200).send(result);
};

export const deleteEmployee = async (req, res) => {
	const { id } = req.body;

	const result = await Employee.delete(id);
	res.status(200).send(result);
};


export const contactSyncRequest = async (req, res) => {
	const { company_id, employee_id } = req.body;
	const [employee, company] = await Promise.all([Employee.getDoc(employee_id), Company.getDoc(company_id)]);

	if (!employee.exists || !company.exists)
		return res.status(400).send('Employee or Company does not exists!');
	if (employee.data()[EmployeeModel.COMPANY_ID] !== company_id)
		return res.status(400).send('Employee does not belong to company!');

	//create local_contacts (||&&) google_contacts sync request 
	const { contact_ids, contacts } = req.body;
	const requestBody = {
		[ContactSyncRequestModel.COMPANY_ID]: company_id,
		[ContactSyncRequestModel.EMPLOYEE_ID]: employee_id,
		[ContactSyncRequestModel.SYNC_STATUS]: 'waiting'
	}

	if (contact_ids && contacts) {
		const [localContactReq, googleContactReq] = await Promise.all([
			ContactSyncRequest.add({
				[ContactSyncRequestModel.CONTACT_IDS]: contact_ids, ...requestBody
			}),
			ContactSyncRequest.add({
				[ContactSyncRequestModel.CONTACTS]: contacts, ...requestBody
			})
		]);

		return res.send({
			localContactReq: localContactReq.id,
			googleContactReq: googleContactReq.id
		});
	}

	if (contact_ids) {
		const localContactReq = await ContactSyncRequest.add({
			[ContactSyncRequestModel.CONTACT_IDS]: contact_ids,
			...requestBody
		});

		res.send({ localContactReq: localContactReq.id });
	}

	const googleContactReq = await ContactSyncRequest.add({
		[ContactSyncRequestModel.CONTACTS]: contacts,
		...requestBody
	});

	res.send({ googleContactReq: googleContactReq.id });
}