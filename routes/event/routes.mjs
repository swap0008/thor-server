import express from 'express';
// import auth from '../../middlewares/auth.mjs';

const router = express.Router();

//Controllers
import { 
   getEvent,
   getEvents,
   saveEvent,
   deleteEvent,
   updateEvent
} from './controllers.mjs';

router.post('/all', getEvents);
router.post('/', getEvent);
router.post('/save', saveEvent);
router.put('/', updateEvent);
router.delete('/', deleteEvent);

export default router;