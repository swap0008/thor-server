import _ from 'lodash';
import EventModel from '../../models/EventModel.mjs';
import DayModel from '../../models/DayModel.mjs';
import { saveSchema, updateSchema, deleteSchema } from '../../schemas/EventSchema.mjs';

const Event = new EventModel();
const Day = new DayModel();

export const getEvent = async (req, res) => {
	const { id } = req.body;
	const event = await Event.getDoc(id);
	res.status(200).send(event.data());
}

export const getEvents = async (req, res) => {
	const { dayId } = req.body;

	const result = await Promise.all([Day.getDoc(dayId), Event.collectionRef.where('day_id', '==', dayId).get()]);

	const { rows = [] } = result[0].data();
	const allEvents = {};
	result[1].docs.forEach((doc) => allEvents[doc.id] = doc.data());

	const data = [];
	rows.forEach(({ position, events, columns }) => {
		if (!data[position]) data[position] = {};
		if (!data[position].columns) data[position].columns = [];

		events.forEach(eventInfo => {
			const event = allEvents[eventInfo.id];

			let column = data[position].columns[eventInfo.column];
			if (!column) column = {};

			if (!column.events) column.events = [];
			column.events[eventInfo.position] = event;

			data[position].columns[eventInfo.column] = column;
		});

		for (let i = 0; i < columns; i++) {
			let column = data[position].columns[i];
			if (!column) {
				column = { events: [] };
				data[position].columns[i] = column;
			}
		}
	});

	res.status(200).send(data);
};

export const saveEvent = async (req, res) => {
	const data = _.pick(req.body, [
		EventModel.TYPE,
		EventModel.NAME,
		EventModel.COLUMN,
		EventModel.POSITION,
		EventModel.DAY_ID,
		EventModel.ROW
	]);

	const { error } = saveSchema(data);
	if (error) return res.status(400).send(error.details[0].message);

	const day = await Day.getDoc(data[EventModel.DAY_ID]);
	const { rows } = day.data();
	const row = rows.filter(row => row.position === data[EventModel.ROW])[0];

	if (!row) return res.status(400).send(`Row number doesn't exist!`);
	if (data[EventModel.COLUMN] + 1 > row[DayModel.COLUMNS]) return res.status(400).send(`Column number doesn't exist!`);

	const eventsInfo = row[DayModel.EVENTS].filter(eventInfo => eventInfo.column === data[EventModel.COLUMN]);
	if (data[EventModel.POSITION] > eventsInfo.length) return res.status(400).send(`Position number invalid!`);

	if (isNaN(data[EventModel.POSITION])) {
		data[EventModel.POSITION] = eventsInfo.length;
	}

	const eventRef = await Event.collectionRef.doc();

	let updatedRows = [];

	const newEventInfo = {
		id: eventRef.id,
		column: data[EventModel.COLUMN],
		position: data[EventModel.POSITION]
	}

	if (row[DayModel.EVENTS].length === 0) {
		updatedRows.push(newEventInfo);
	} else if (eventsInfo.length === data[EventModel.POSITION]) {
		updatedRows = row[DayModel.EVENTS];
		updatedRows.push(newEventInfo);
	} else {
		row[DayModel.EVENTS].forEach(eventInfo => {
			if (eventInfo.column !== data[EventModel.COLUMN]) {
				updatedRows.push(eventInfo);
			} else {
				if (eventInfo.position === data[EventModel.POSITION]) {
					updatedRows.push(newEventInfo);
				}

				if (eventInfo.position >= data[EventModel.POSITION]) eventInfo.position++;
				updatedRows.push(eventInfo);
			}
		});
	}

	rows.every(row => {
		if (row.position === data[EventModel.ROW]) {
			row[DayModel.EVENTS] = updatedRows;
			return false;
		};
		return true;
	});
	
	delete data[EventModel.ROW];
	delete data[EventModel.POSITION];
	delete data[EventModel.COLUMN];

	await Promise.all([
		Day.update(data[EventModel.DAY_ID], { rows }),
		Event.set(eventRef.id, { id: eventRef.id, ...data })
	]);
	res.status(200).send(eventRef.id);
};

export const updateEvent = async (req, res) => {
	const data = _.pick(req.body, [EventModel.TYPE, EventModel.NAME]);

	const { error } = updateSchema(data);
	if (error) return res.status(400).send(error.details[0].message);

	const { id } = req.body;

	const result = await Event.update(id, data);
	res.status(200).send(result);
};

export const deleteEvent = async (req, res) => {
	const data = _.pick(req.body, [
		EventModel.ID,
		EventModel.COLUMN,
		EventModel.POSITION,
		EventModel.DAY_ID,
		EventModel.ROW
	]);

	const { error } = deleteSchema(data);
	if (error) return res.status(400).send(error.details[0].message);

	const day = await Day.getDoc(data[EventModel.DAY_ID]);
	const { rows } = day.data();
	const row = rows[data[EventModel.ROW]];

	let updatedRows = [];
	row[DayModel.EVENTS].forEach(eventInfo => {
		if (eventInfo.column !== data[EventModel.COLUMN]) {
			updatedRows.push(eventInfo);
		} else {
			if (eventInfo.position === data[EventModel.POSITION]) {
				return;
			}

			if (eventInfo.position >= data[EventModel.POSITION]) eventInfo.position--;
			updatedRows.push(eventInfo);
		}
	});

	rows[data[EventModel.ROW]][DayModel.EVENTS] = updatedRows;

	await Promise.all([
		Day.update(data[EventModel.DAY_ID], { rows }),
		Event.delete(data[EventModel.ID])
	]);
	res.status(200).send(data[EventModel.ID]);
};
