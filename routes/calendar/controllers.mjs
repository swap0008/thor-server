import _ from 'lodash';
import uuid from 'uuid';
import GoogleApis from 'googleapis';
import randomColor from 'randomcolor';
import credentials from '../google_oauth/credentials.mjs';
import OAuth2Model from '../../models/OAuth2Model.mjs';
import CalendarModel from '../../models/CalendarModel.mjs';
import CalendarEventModel from '../../models/CalendarEventModel.mjs';

const Calendar = new CalendarModel();
const CalendarEvent = new CalendarEventModel();
const OAuth2 = new OAuth2Model();

const { google } = GoogleApis;
const { client_secret, client_id, redirect_uris } = credentials.web;

const mapToTuiEvent = (event, calendarId) => {
    return {
        id: event.id,
        calendarId: calendarId,
        start: event.start.dateTime,
        end: event.end.dateTime,
        title: event.summary,
        location: event.location || '',
        category: 'time'
    };
}

const mapToGoogleEvent = (event) => {
    return {
        summary: event.title,
        location: event.location,
        end: {
            dateTime: new Date(event.end._date).toISOString()
        },
        start: {
            dateTime: new Date(event.start._date).toISOString()
        }
    };
}

const mapToTuiCalendar = (calendar, type) => {
    return {
        id: `${calendar.id}:-:${type}`,
        name: calendar.summary,
        bgColor: calendar.backgroundColor || 'blue',
        color: calendar.foregroundColor || 'white',
        description: calendar.description || ''
    };
}

const getCalendarColor = () => {
    return {
        backgroundColor: randomColor({ luminosity: 'bright' }),
        foregroundColor: 'black'
    }
}

const commonChecks = async (req, res) => {
    const companyId = req.body[OAuth2Model.COMPANY_ID];
    const employeeId = req.body[OAuth2Model.EMPLOYEE_ID];

    let query = OAuth2.collectionRef.where(OAuth2Model.COMPANY_ID, '==', companyId);
    if (companyId && employeeId) query = query.where(OAuth2Model.EMPLOYEE_ID, '==', employeeId);
    else query = query.where(OAuth2Model.EMPLOYEE_ID, '==', 'company');

    const snapshot = await query.get();
    if (snapshot.empty) return res.status(400).send('No matching document.');

    const { google_accounts } = snapshot.docs.map(doc => doc.data())[0];

    const gmails = req.body[OAuth2Model.GOOGLE_ACCOUNTS] || [];

    const services = [];
    gmails.forEach(gmail => {
        if (!google_accounts[gmail]) return;

        const { calendar: { refresh_token } } = google_accounts[gmail];

        const oAuth2Client = new google.auth.OAuth2(client_id, client_secret, redirect_uris[3]);
        oAuth2Client.setCredentials({ refresh_token });

        // const service = google.calendar({ version: 'v3', auth: oAuth2Client });
        // service.events.watch({
        //     calendarId:
        //     resource: {}
        // })

        services.push(google.calendar({ version: 'v3', auth: oAuth2Client }));
    });

    return services;
}

//Events Helpers
const getEventsList = async (service, calendar_id) => {
    const { data: { items } } = await service.events.list({
        calendarId: calendar_id || 'primary'
    });

    // service.events.watch({
    //     calendarId: 'temporary@mdcpartners.be',
    //     resource: {
    //         id: uuid.v1(),
    //         type: 'web_hook',
    //         address: 'https://rebrand.ly/thor-server'
    //     }
    // }).then(res => console.log(res))
    //     .catch(err => console.log(err));

    return items;
}

const createEvent = async (service, calendar_id, event) => {
    const { data } = await service.events.insert({
        calendarId: calendar_id || 'primary',
        resource: {
            ...event
        }
    });

    return data;
}

const deleteEvent = async (service, calendar_id, event_id) => {
    const data = await service.events.delete({
        calendarId: calendar_id,
        eventId: event_id
    });

    return data;
}

const updateEvent = async (service, calendar_id, event) => {
    const { data } = await service.events.update({
        calendarId: calendar_id,
        eventId: event.id,
        resource: {
            ...event
        }
    });

    return data;
}

//Calendar Helpers
export const getCalendarList = async (service) => {
    const { data: { items } } = await service.calendarList.list();

    return items;
}

export const createCalendar = async (service, calendar) => {
    const data = await service.calendars.insert({
        resource: {
            ...calendar
        }
    });

    return data;
}

export const removeCalendar = async (service, calendar_id) => {
    const data = await service.calendars.delete({
        calendarId: calendar_id
    });

    return data;
}

export const updateCalendar = async (service, calendar) => {
    const data = await service.calendars.update({
        calendarId: calendar.id,
        resource: {
            ...calendar
        }
    });

    return data;
}


//Events Controllers
export const getCalendarEvents = async (req, res) => {
    const { calendar_ids, type } = req.body;

    let allEvents = {};

    if (type === 'platform') {
        const snapshot = await CalendarEvent.collectionRef.where('calendar_id', '==', calendar_ids[0]).get();
        snapshot.docs.map(doc => allEvents[doc.id] = mapToTuiEvent({ id: doc.id, ...doc.data() }, `${calendar_ids[0]}:-:platform`));
        return res.send(allEvents);
    }

    const services = await commonChecks(req, res);

    for (let calendar_id of calendar_ids) {
        const events = await getEventsList(services[0], calendar_id);
        events.forEach(event => allEvents[event.id] = mapToTuiEvent(event, `${calendar_id}:-:google`));
    }

    res.send(allEvents);
}

export const createCalendarEvent = async (req, res) => {
    let { event, calendar_id, type } = req.body;
    event = mapToGoogleEvent(event);

    if (type === 'platform') {
        event['calendar_id'] = calendar_id;
        const doc = await CalendarEvent.add(event);

        event.id = doc.id;
        return res.send(mapToTuiEvent(event, calendar_id));
    }

    const services = await commonChecks(req, res);

    const { id } = await createEvent(services[0], calendar_id, event);

    event = mapToTuiEvent({ ...event, id }, calendar_id);
    res.send(event);
}

export const deleteCalendarEvent = async (req, res) => {
    const { calendar_id, event_id, type } = req.body;

    if (type === 'platform') {
        await CalendarEvent.delete(event_id);
        res.send(event_id);
    }

    const services = await commonChecks(req, res);
    const result = await deleteEvent(services[0], calendar_id, event_id);

    res.send(result);
}

export const updateCalendarEvent = async (req, res) => {
    let { event, calendar_id, from, to, type } = req.body;

    if (type === 'platform') {
        if (from && to) {
            await Promise.all([
                CalendarEvent.delete(event.id),
                CalendarEvent.update(event.id, { calendar_id: to })
            ]);

            event = mapToGoogleEvent(event);
            return res.send(mapToTuiEvent(event, `${calendar_id}:-:platform`));
        }

        await CalendarEvent.update(event.id, mapToGoogleEvent(event));
        event = mapToGoogleEvent(event);
        return res.send(mapToTuiEvent(event, `${calendar_id}:-:platform`));
    }

    const services = await commonChecks(req, res);

    const eventId = event.id || '';
    if (from && to) {
        event = mapToGoogleEvent(event);

        const { id } = await createEvent(services[0], to, event);
        await deleteEvent(services[0], from, eventId);

        event = mapToTuiEvent({ ...event, id }, to);
        return res.send(event);
    }

    event = { id: event.id, ...mapToGoogleEvent(event) };
    await updateEvent(services[0], calendar_id, event);

    event = mapToTuiEvent(event, `${calendar_id}:-:google`);
    res.send(event);
}


//Calendar Controllers
export const getCalendars = async (req, res) => {
    const services = await commonChecks(req, res);

    const calendars = {};
    for (const service of services) {
        const list = await getCalendarList(service);
        list.forEach(calendar => calendars[`${calendar.id}:-:google`] = mapToTuiCalendar(calendar, 'google'));
    }

    const platformCalendars = await Calendar.collectionRef.get();
    platformCalendars.docs.map(doc => calendars[`${doc.id}:-:platform`] = mapToTuiCalendar({ ...doc.data(), id: doc.id }, 'platform'));

    res.send(calendars);
}

export const createNewCalendar = async (req, res) => {
    let { calendar, type } = req.body;

    if (type === 'platform') {
        calendar = {
            ...calendar,
            ...getCalendarColor()
        }
        const doc = await Calendar.add(calendar);

        calendar.id = doc.id;
        return res.send(mapToTuiCalendar(calendar, 'platform'));
    }

    const services = await commonChecks(req, res);

    let { data } = await createCalendar(services[0], calendar);

    res.send(mapToTuiCalendar(data, 'google'));
}

export const deleteCalendar = async (req, res) => {
    const { calendar_id, type } = req.body;

    if (type === 'platform') {
        const events = await CalendarEvent.collectionRef.where(CalendarEventModel.CALENDAR_ID, '==', calendar_id).get();
        // if (events.empty) return res.status(400).send('No events found');

        await Promise.all([
            Calendar.delete(calendar_id),
            CalendarEvent.deleteMultipleDoc([''], events)
        ]);

        return res.send(calendar_id);
    }

    const services = await commonChecks(req, res);
    const result = await removeCalendar(services[0], calendar_id);

    res.send(result);
}

export const updateCalendarInfo = async (req, res) => {
    const { calendar, type, moveToGoogle } = req.body;

    if (type === 'platform') {
        const { id } = calendar;
        await Calendar.update(id, calendar);
        return res.send(mapToTuiCalendar(calendar, 'platform'));
    }

    const services = await commonChecks(req, res);

    if (moveToGoogle) {
        const events = await CalendarEvent.collectionRef.where('calendar_id', '==', calendar.id).get();
        const calendarId = calendar.id;
        delete calendar.id;
        let { data } = await createCalendar(services[0], calendar);

        const allEvents = {};
        for (let event of events.docs) {
            event = event.data();
            delete event.calendar_id;
            const { id } = await createEvent(services[0], data.id, event);
            event = mapToTuiEvent({ ...event, id }, `${data.id}:-:google`);

            allEvents[event.id] = event;
        }

        await Promise.all([
            Calendar.delete(calendarId),
            CalendarEvent.deleteMultipleDoc([''], events)
        ]);

        console.log(data);

        return res.send({ calendar: mapToTuiCalendar(data, 'google'), events: allEvents });
    }

    await updateCalendar(services[0], calendar);

    res.send(mapToTuiCalendar(calendar, 'google'));
}