import express from 'express';

const router = express.Router();

import { 
    getCalendarEvents, 
    getCalendars, 
    createCalendarEvent,
    deleteCalendarEvent,
    updateCalendarEvent,
    createNewCalendar,
    deleteCalendar,
    updateCalendarInfo
} from './controllers.mjs';

router.post('/list', getCalendars);
router.post('/events', getCalendarEvents);

router.post('/', createNewCalendar);
router.delete('/', deleteCalendar);
router.put('/', updateCalendarInfo);

router.post('/event', createCalendarEvent);
router.delete('/event', deleteCalendarEvent);
router.put('/event', updateCalendarEvent);

export default router;