import _ from 'lodash';
import ItineraryModel from '../../models/ItineraryModel.mjs';
import { saveSchema, updateSchema } from '../../schemas/ItinerarySchema.mjs';

const Itinerary = new ItineraryModel();

export const getItinerary = async (req, res) => {
	const { id } = req.body;
	const itinerary = await Itinerary.getDoc(id);
	res.status(200).send(itinerary.data());
}

export const getItineraries = async (req, res) => {
	const Itineraries = await Itinerary.get();
	res.status(200).send(Itineraries);
};

export const saveItinerary = async (req, res) => {
	const data = _.pick(req.body, [
		ItineraryModel.COMPANY_ID,
		ItineraryModel.EMPLOYEE_ID,
		ItineraryModel.NAME,
		ItineraryModel.SMALL_DESCRIPTION,
		ItineraryModel.LANGUAGE,
		ItineraryModel.CURRENCY,
		ItineraryModel.COST
	]);

	const { error } = saveSchema(data);
	if (error) return res.status(400).send(error.details[0].message);

	const result = await Itinerary.add(data);
	res.status(200).send(result.id);
};

export const updateItinerary = async (req, res) => {
	const data = _.pick(req.body, [
		ItineraryModel.COMPANY_ID,
		ItineraryModel.EMPLOYEE_ID,
		ItineraryModel.NAME,
		ItineraryModel.SMALL_DESCRIPTION,
		ItineraryModel.LANGUAGE,
		ItineraryModel.CURRENCY,
		ItineraryModel.COST
	]);

	const { error } = updateSchema(data);
	if (error) return res.status(400).send(error.details[0].message);

	const { id } = req.body;

	const result = await Itinerary.update(id, data);
	res.send(result);
};

export const deleteItinerary = async (req, res) => {
	const { id } = req.body;

	const result = await Itinerary.delete(id);
	res.status(200).send(result);
};
