import express from 'express';
// import auth from '../../middlewares/auth.mjs';

const router = express.Router();

//Controllers
import { 
   getItinerary,
   getItineraries,
   saveItinerary,
   deleteItinerary,
   updateItinerary
} from './controllers.mjs';

router.get('/all', getItineraries);
router.post('/', getItinerary);
router.post('/save', saveItinerary);
router.put('/', updateItinerary);
router.delete('/', deleteItinerary);

export default router;