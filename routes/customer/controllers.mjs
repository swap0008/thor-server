import _ from 'lodash';
import CustomerModel from '../../models/CustomerModel.mjs';
import { saveSchema, updateSchema } from '../../schemas/CustomerSchema.mjs';

const Customer = new CustomerModel();

export const getCustomers = async (req, res) => {
	const Customers = await Customer.get();
	res.status(200).send(Customers);
};

export const saveCustomer = async (req, res) => {
	const data = _.pick(req.body, [
		CustomerModel.NAME,
		CustomerModel.EMAIL,
		CustomerModel.PHONE,
		CustomerModel.COUNTRY,
		CustomerModel.STATE,
		CustomerModel.CITY
	]);

	const { error } = saveSchema(data);
	if (error) return res.status(400).send(error.details[0].message);

	const result = await Customer.set(data[CustomerModel.EMAIL], data);
	res.status(200).send(result);
};

export const updateCustomer = async (req, res) => {
	const data = _.pick(req.body, [
		CustomerModel.NAME,
		CustomerModel.EMAIL,
		CustomerModel.PHONE,
		CustomerModel.COUNTRY,
		CustomerModel.STATE,
		CustomerModel.CITY
	]);

	const { error } = updateSchema(data);
	if (error) return res.status(400).send(error.details[0].message);

	const { id } = req.body;

	const result = await Customer.update(id, data);
	res.send(result);
};

export const deleteCustomer = async (req, res) => {
	const { id } = req.body;

	const result = await Customer.delete(id);
	res.status(200).send(result);
};
