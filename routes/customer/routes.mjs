import express from 'express';
// import auth from '../../middlewares/auth.mjs';

const router = express.Router();

//Controllers
import { 
   getCustomers,
   saveCustomer,
   deleteCustomer,
   updateCustomer
} from './controllers.mjs';

router.get('/all', getCustomers);
router.post('/', saveCustomer);
router.put('/', updateCustomer);
router.delete('/', deleteCustomer);

export default router;