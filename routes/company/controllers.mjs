import _ from 'lodash';
import CompanyModel from '../../models/CompanyModel.mjs';
import ContactSyncRequestModel from '../../models/ContactSyncRequestModel.mjs';
import { saveSchema, updateSchema } from '../../schemas/CompanySchema.mjs';
import { syncContacts } from '../contact/controllers.mjs';

const Company = new CompanyModel();
const ContactSyncRequest = new ContactSyncRequestModel();

export const getCompanies = async (req, res) => {
	const Companies = await Company.get();
	res.status(200).send(Companies);
};

export const saveCompany = async (req, res) => {
	const data = _.pick(req.body, [
		CompanyModel.NAME,
		CompanyModel.EMAIL,
		CompanyModel.PHONE,
		CompanyModel.COUNTRY,
		CompanyModel.STATE,
		CompanyModel.CITY
	]);

	data[CompanyModel.LANGUAGES] = ['en'];
	data[CompanyModel.PRIMARY_DESTINATIONS] = [];

	const { error } = saveSchema(data);
	if (error) return res.status(400).send(error.details[0].message);

	const { email } = req.body;

	const result = await Company.set(email, data);
	res.status(200).send(result);
};

export const updateCompany = async (req, res) => {
	const data = _.pick(req.body, [
		CompanyModel.NAME,
		CompanyModel.EMAIL,
		CompanyModel.PHONE,
		CompanyModel.COUNTRY,
		CompanyModel.STATE,
		CompanyModel.CITY
	]);
	const arrayData = _.pick(req.body, [CompanyModel.LANGUAGES, CompanyModel.PRIMARY_DESTINATIONS]);

	const { error } = updateSchema({ ...data, ...arrayData });
	if (error) return res.status(400).send(error.details[0].message);

	const { id } = req.body;

	if (!_.isEmpty(arrayData) && !_.isEmpty(data)) {
		const result = await Promise.all([Company.update(id, data), Company.updateArrayUnion(id, arrayData)]);
		res.status(200).send(result);
		return;
	} else if (!_.isEmpty(arrayData)) {
		const result = await Company.updateArrayUnion(id, arrayData);
		res.status(200).send(result);
		return;
	} else if (!_.isEmpty(data)) {
		const result = await Company.update(id, data);
		res.send(result);
		return;
	}

	res.status(200).send('Nothing to update!');
};

export const deleteCompany = async (req, res) => {
	const { id } = req.body;

	const result = await Company.delete(id);
	res.status(200).send(result);
};

export const contactSyncRequest = async (req, res) => {
	//Check Contact Sync Request
	const { company_id, employee_id } = req.body;
	const query = ContactSyncRequest.collectionRef
		.where(ContactSyncRequestModel.COMPANY_ID, '==', company_id)
		.where(ContactSyncRequestModel.EMPLOYEE_ID, '==', employee_id);

	const snapshot = await query.get();

	if (snapshot.empty)
		return res.status(400).send('No contact sync request found.');

	// Approve or Reject Contact Sync Request
	const { sync_status, request_id } = req.body;
	const syncRequests = {};
	snapshot.docs.forEach(doc => syncRequests[doc.id] = doc.data());

	if (sync_status === 'rejected' && request_id) {
		if (!syncRequests[request_id]) return res.status(400).send('Request ID does not exists!');

		await ContactSyncRequest.update(request_id, { status });
		return res.send('Status set to rejected!');
	}

	if (sync_status === 'approved' && request_id) {
		if (!syncRequests[request_id]) return res.status(400).send('Request ID does not exists!');

		const syncRequest = syncRequests[request_id];

		if (syncRequest[ContactSyncRequestModel.SYNC_STATUS] === 'done') return res.status(400).send('Already synced!');

		await ContactSyncRequest.update(request_id, { sync_status });

		//get company default account
		let company = await Company.getDoc(company_id);
		if (!company.exists) return res.status(400).send('Company does not exists!');
		company = doc.data();
		req.body['google_accounts'] = company[CompanyModel.DEFAULT_CONTACTS_SYNC_TO];
		delete req.body['employee_id'];

		//Sync local contacts
		if (syncRequest[ContactSyncRequestModel.CONTACT_IDS]) {
			req.body[ContactSyncRequestModel.CONTACT_IDS] = syncRequest[ContactSyncRequestModel.CONTACT_IDS];
			return await syncContacts(req, res);
		}

		//sync google contacts
		if (syncRequest[ContactSyncRequestModel.CONTACTS]) {
			req.body['google_contacts'] = syncRequest[ContactSyncRequestModel.CONTACTS];
			return await syncContacts(req, res);
		}
	}

	res.send(syncRequests);
}