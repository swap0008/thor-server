import express from 'express';
// import auth from '../../middlewares/auth.mjs';

const router = express.Router();

//Controllers
import {
   getCompanies,
   saveCompany,
   deleteCompany,
   updateCompany,
   contactSyncRequest
} from './controllers.mjs';

//Routes
/**
 * @swagger 
 * /company/all:
 *    get:
 *       description: Get list of all companies
 *       responses: 
 *           '200':
 *                description: A successful response
 * 
 * /company:
 *    post:
 *       description: Save company
 *       response:
 *          '200':
 *             descrption: Company Saved
 *       parameters:
 *        - name: name
 *          in: path
 *          type: string
 *        - name: email
 *          in: path
 *          type: string
 *        - name: phone
 *          in: path
 *          type: string
 *        - name: country
 *          in: path
 *          type: string
 *        - name: state
 *          in: path
 *          type: string
 *        - name: city
 *          in: path
 *          type: string
 */
router.get('/all', getCompanies);
router.post('/', saveCompany);
router.put('/', updateCompany);
router.delete('/', deleteCompany);
router.post('/contacts_sync_requests', contactSyncRequest);

export default router;