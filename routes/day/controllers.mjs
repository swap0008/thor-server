import _ from 'lodash';
import DayModel from '../../models/DayModel.mjs';
import EventModel from '../../models/EventModel.mjs';

const Day = new DayModel();
const Event = new EventModel();

export const getDays = async (req, res) => {
    const days = await Day.get();
    res.status(200).send(days);
}

export const updateRow = async (data, res) => {
    if (data.row && data.type === 'add') {
        data.row.events = [];
    }

    const day = await Day.getDoc(data.id);
    const { rows } = day.data();

    let updatedRows = [];

    if (data.type === 'delete') {
        if (!rows[data.row.position]) return res.status(400).send(`Row doesn't exist!`);
        rows.forEach(row => {
            if (row.position === data.row.position) {
                return;
            }

            if (row.position >= data.row.position) {
                row.position--;
            }
            updatedRows.push(row);
        });
        const eventsId = rows[data.row.position].events.map(event => event.id);
        await Promise.all([
            Day.update(data.id, { rows: updatedRows }),
            Event.deleteMultipleDoc(eventsId)
        ]);
        res.send('deleted');
    } else if (data.type === 'add') {
        if (rows.length === 0) updatedRows.push(data.row);
        if (data.row.position === rows.length) {
            updatedRows = rows;
            updatedRows.push(data.row);
        } else {
            rows.forEach(row => {
                if (row.position === data.row.position) {
                    updatedRows.push(data.row);
                }

                if (row.position >= data.row.position) row.position++;
                updatedRows.push(row);
            });
        }
        await Day.update(data.id, { rows: updatedRows });
        res.status(200).send('added');
    } else if (data.type === 'move_up') {
        if (rows.length === 1) return res.status(400).send('Nothing to move!');
        const position = data.row.position;
        if (position === 0) return res.status(400).send('Cannot move!');

        rows.forEach(row => {
            if (row.position === position) {
                row.position--;
            } else if (row.position === position - 1) {
                row.position++;
            }
            updatedRows.push(row);
        });

        await Day.update(data.id, { rows: updatedRows });
        res.send(rows);
    } else if (data.type === 'move_down') {
        if (rows.length === 1) return res.status(400).send('Nothing to move!');
        const position = data.row.position;
        if (position >= rows.length - 1) return res.status(400).send('Cannot move!');

        rows.forEach(row => {
            if (row.position === position) {
                row.position++;
            } else if (row.position === position + 1) {
                row.position--;
            }
            updatedRows.push(row);
        });

        await Day.update(data.id, { rows: updatedRows });
        res.send('Moved down!');
    } else if (data.type.includes('move_event')) {
        const row = rows.filter(row => row.position === data[EventModel.ROW].number)[0];
        const column = data['row'].column, position = data['row'].position;
        const events = row[DayModel.EVENTS].filter(event => event.column === column);

        if (data.type === 'move_event_up') {
            if (position === 0 || events.length === 1) return res.status(400).send('Cannot move up!');

            row[DayModel.EVENTS].forEach(event => {
                if (event.position === position) {
                    event.position--;
                } else if (event.position === position - 1) {
                    event.position++;
                }
            });
        }

        if (data.type === 'move_event_down') {
            if (position >= events.length - 1 || events.length === 1) return res.status(400).send('Cannot move down!');

            row[DayModel.EVENTS].forEach(event => {
                if (event.position === position) {
                    event.position++;
                } else if (event.position === position + 1) {
                    event.position--;
                }
            });
        }

        rows.every(row => {
            if (row.position === data[EventModel.ROW]) {
                row[DayModel.EVENTS] = row;
                return false;
            };
            return true;
        });

        await Day.update(data.id, { rows });
        res.send('Moved!');
    }
}

export const updateDay = async (req, res) => {
    const data = _.pick(req.body, ['id', 'row', 'type']);
    updateRow(data, res);
}