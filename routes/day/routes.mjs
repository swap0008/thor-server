import express from 'express';
// import auth from '../../middlewares/auth.mjs';

const router = express.Router();

//Controllers
import { 
   getDays,
   updateDay
} from './controllers.mjs';

router.get('/all', getDays);
router.put('/', updateDay);

export default router;