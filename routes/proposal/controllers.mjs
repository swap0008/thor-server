import _ from 'lodash';
import ProposalModel from '../../models/ProposalModel.mjs';
import { saveSchema, updateSchema } from '../../schemas/ProposalSchema.mjs';

const Proposal = new ProposalModel();

export const getProposals = async (req, res) => {
    const proposals = await Proposal.get();
    res.send(proposals);
}

export const saveProposal = async (req, res) => {
    const data = _.pick(req.body, [
        [ProposalModel.NAME],
        [ProposalModel.CLIENT],
        [ProposalModel.DURATION],
        [ProposalModel.COST],
        [ProposalModel.DATES],
        [ProposalModel.STATUS],
        [ProposalModel.COMPANY_ID],
        [ProposalModel.EMPLOYEE_ID]
    ]);

    const { error } = saveSchema(data);
	if (error) return res.status(400).send(error.details[0].message);

    const result = await Proposal.add(data);
    res.send(result.id);
}

export const updateProposal = async (req, res) => {
    const data = _.pick(req.body, [
        [ProposalModel.NAME],
        [ProposalModel.CLIENT],
        [ProposalModel.DURATION],
        [ProposalModel.COST],
        [ProposalModel.DATES],
        [ProposalModel.STATUS],
        [ProposalModel.COMPANY_ID],
        [ProposalModel.EMPLOYEE_ID]
    ]);

    const { error } = updateSchema(data);
    if (error) return res.status(400).send(error.details[0].message);
    
    const { id } = req.body;

    const result = await Proposal.update(id, data);
    res.send(result);
}

export const deleteProposal = async (req, res) => {
    const result = await Proposal.delete(req.body.id);
    res.send(result);
}