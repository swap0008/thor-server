import express from 'express';
// import auth from '../../middlewares/auth.mjs';

const router = express.Router();

//Controllers
import { 
    getProposals,
    saveProposal,
    deleteProposal,
    updateProposal
} from './controllers.mjs';

router.get('/all', getProposals);
router.post('/', saveProposal);
router.put('/', updateProposal);
router.delete('/', deleteProposal);

export default router;