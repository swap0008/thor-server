import FirebaseClient from './FirebaseClient.mjs';
import ItinerarySchema from '../schemas/ItinerarySchema.mjs';

class ItineraryModel extends FirebaseClient {
    static NAME = 'name';
    static SMALL_DESCRIPTION = 'small_description';
    static LANGUAGE = 'language';
    static CURRENCY = 'currency'; 
    static COST = 'cost'; 
    static COMPANY_ID = 'company_id';
    static EMPLOYEE_ID = 'employee_id';

    constructor() {
        super('itineraries', ItinerarySchema);
    }
}

export default ItineraryModel;