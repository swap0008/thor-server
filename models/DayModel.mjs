import FirebaseClient from './FirebaseClient.mjs';

class DayModel extends FirebaseClient {
    static NAME = 'name';
    static ROWS = 'rows';
    static COLUMNS = 'columns';
    static EVENTS = 'events';
   
    constructor() {
       super('days'); 
    }
}

export default DayModel;