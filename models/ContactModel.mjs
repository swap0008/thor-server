import FirebaseClient from './FirebaseClient.mjs';

class ContactModel extends FirebaseClient {
    static FIRST_NAME = 'first_name';
    static LAST_NAME = 'last_name';
    static PHONE_NUMBERS = 'phone_numbers';
    static DEFAULT_PHONE = 'default_phone';
    static EMAIL_ADDRESSES = 'email_addresses';
    static ADDRESS = 'address';
    static NOTES = 'notes';
    static TYPE = 'type';
    static COMPANY_ID = 'company_id';
    static EMPLOYEE_ID = 'employee_id';
    static ID = 'id';

    constructor() {
        super('contacts');
    }
}

export default ContactModel;