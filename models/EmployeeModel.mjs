import jwt from 'jsonwebtoken';
import config from 'config';
import FirebaseClient from './FirebaseClient.mjs';
import EmployeeSchema from '../schemas/EmployeeSchema.mjs';

class EmployeeModel extends FirebaseClient {
    static COMPANY_ID = 'company_id';
    static NAME = 'name';
    static EMAIL = 'email';

    constructor() {
        super('employees', EmployeeSchema);
    }

    generateToken(id) {
        const token = jwt.sign({ id: id }, config.get('jwtPrivateKey'));
        return token;
    }
}

export default EmployeeModel;