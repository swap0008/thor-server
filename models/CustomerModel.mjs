import FirebaseClient from './FirebaseClient.mjs';
import CustomerSchema from '../schemas/CustomerSchema.mjs';

class CustomerModel extends FirebaseClient {
    static NAME = 'name';
    static EMAIL = 'email';
    static PHONE = 'phone';
    static COUNTRY = 'country';
    static STATE = 'state';
    static CITY = 'city';

    constructor() {
        super('customers', CustomerSchema);
    }
}

export default CustomerModel;