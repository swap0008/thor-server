import admin from 'firebase-admin';
import serviceAccount from '../startup/application_config.mjs';

admin.initializeApp({
	credential: admin.credential.cert(serviceAccount)
});

const db = admin.firestore();

class FirebaseClient {
	constructor(collectionName, schema = {}) {
		this.collectionRef = db.collection(collectionName);
		this.schema = schema;
	}

	getDoc = async (id) => {
		return await this.collectionRef.doc(id).get();
	};

	get = async () => {
		const result = await this.collectionRef.get();
		const data = {};
		result.docs.forEach((doc) => {
			data[doc.id] = { id: doc.id, ...doc.data() };
			return data;
		});
		return data;
	};

	set = async (id, data) => {
		// const { error = false } = this.schema.saveSchema(data);
		// if (error) return error.details[0].message;

		return await this.collectionRef.doc(id).set(data);
	};

	add = async (data) => {
		return await this.collectionRef.add(data);
	};

	update = async (id, data) => {
		// const { error = false } = this.schema.updateSchema(data);
		// if (error) return error.details[0].message;

		return await this.collectionRef.doc(id).update(data);
	};

	updateArrayUnion = async (id, newData) => {
		const { error } = this.schema.updateSchema(newData);
		if (error) return error.details[0].message;

		const ref = await this.collectionRef.doc(id);
		const transaction = await db.runTransaction(async (t) => {
			let doc = await t.get(ref);
			let oldData = doc.data();
			Object.keys(newData).forEach((key) => (newData[key] = [...new Set(oldData[key].concat(newData[key]))]));
			const result = await t.update(ref, newData);
			return Promise.resolve(newData);
		});

		return transaction;
	};

	updateArrayRemove = async (id, field, value) => {
		return await this.collectionRef.doc(id).update({
			[field]: admin.firestore.FieldValue.arrayRemove(value)
		});
	};

	incrementValue = async (id, field) => {
		return await this.collectionRef.doc(id).update({
			[field]: admin.firestore.FieldValue.increment(1)
		});
	};

	decrementValue = async (id, field) => {
		return await this.collectionRef.doc(id).update({
			[field]: admin.firestore.FieldValue.increment(-1)
		});
	};

	delete = async (id) => {
		return await this.collectionRef.doc(id).delete();
	};

	multipleDocQuery = async (ids) => {
		const result = await this.collectionRef.where('day_id', '==', ids).get();
		const data = [];
		result.docs.map(doc => data.push(doc.data()));
		return data;
	}

	deleteMultipleDoc = async (ids, snapshot) => {
		if (!ids.length) return { message: 'ids length empty'};
		if (!snapshot) 
			snapshot = await this.collectionRef.where('id', 'in', ids).get();
		const batch = db.batch();
		snapshot.forEach(doc => batch.delete(doc.ref));
		const result = await batch.commit();
		return result;
	}
}

export default FirebaseClient;
