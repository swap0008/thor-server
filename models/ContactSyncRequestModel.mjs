import FirebaseClient from './FirebaseClient.mjs';

class ContactSyncRequestModel extends FirebaseClient {
    static COMPANY_ID = 'company_id';
    static EMPLOYEE_ID = 'employee_id';
    static CONTACTS = 'contacts';
    static CONTACT_IDS = 'contact_ids';
    static SYNC_STATUS = 'sync_status';

    constructor() {
        super('contacts_sync_requests');
    }
}

export default ContactSyncRequestModel;