import FirebaseClient from './FirebaseClient.mjs';
import ProposalSchema from '../schemas/ProposalSchema.mjs';

class ProposalModel extends FirebaseClient {
	static NAME = 'name';
	static CLIENT = 'client';
	static DURATION = 'duration';
	static COST = 'cost';
	static DATES = 'dates';
	static STATUS = 'status';
    static COMPANY_ID = 'company_id';
    static EMPLOYEE_ID = 'employee_id';

	constructor() {
		super('proposals', ProposalSchema);
	}
}

export default ProposalModel;