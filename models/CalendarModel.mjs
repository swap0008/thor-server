import FirebaseClient from './FirebaseClient.mjs';

class CalendarModel extends FirebaseClient {
    static SUMMARY = 'summary';
    static DESCRIPTION = 'description';
    
    constructor() {
        super('calendar');
    }
}

export default CalendarModel;
