import FirebaseClient from './FirebaseClient.mjs';
import EventSchema from '../schemas/EventSchema.mjs';

class EventModel extends FirebaseClient {
    static ID = 'id';
    static NAME = 'name';
    static TYPE = 'type';
    static DAY_ID = 'day_id';
    static COLUMN = 'column';
    static POSITION = 'position';
    static ROW = 'row';

    constructor() {
        super('events', EventSchema);
    }
}

export default EventModel;