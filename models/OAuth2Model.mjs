import FirebaseClient from './FirebaseClient.mjs';

class OAuth2Model extends FirebaseClient {
    static COMPANY_ID = 'company_id';
    static EMPLOYEE_ID = 'employee_id';
    static GOOGLE_ACCOUNTS = 'google_accounts';

    constructor() {
        super('oauth2');
    }
}

export default OAuth2Model;