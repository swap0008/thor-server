import jwt from 'jsonwebtoken';
import config from 'config';
import FirebaseClient from './FirebaseClient.mjs';
import CompanySchema from '../schemas/CompanySchema.mjs';

class CompanyModel extends FirebaseClient {
	static NAME = 'name';
	static EMAIL = 'email';
	static PHONE = 'phone';
	static COUNTRY = 'country';
	static STATE = 'state';
	static CITY = 'city';
	static LANGUAGES = 'languages';
	static PRIMARY_DESTINATIONS = 'primary_destinations';
	static DEFAULT_CONTACTS_SYNC_TO = 'default_contacts_sync_to';

	constructor() {
		super('companies', CompanySchema);
	}

	generateToken(id) {
		const token = jwt.sign({ id: id }, config.get('jwtPrivateKey'));
		return token;
	}
}

export default CompanyModel;
