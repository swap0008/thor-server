import FirebaseClient from './FirebaseClient.mjs';

class CalendarEventModel extends FirebaseClient {
    static SUMMARY = 'summary';
    static DESCRIPTION = 'description';
    static LOCATION = 'location';
    static END = 'end';
    static START  = 'start';
    static CALENDAR_ID = 'calendar_id';

    constructor() {
        super('calendar_events');
    }
}

export default CalendarEventModel;