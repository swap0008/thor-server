import jwt from 'jsonwebtoken';
import config from 'config';
import Company from '../models/CompanyModel.mjs';

export default async function (req, res, next) {
    const token = req.header('x-auth-token');

    if(!token) return res.status(401).send('Access denied. No token provided');

    try {
        const { id } = jwt.verify(token, config.get('jwtPrivateKey'));

        const result = await Company.collectionRef.doc(id).get();
        if (!result.exists) return res.status(404).send('Company not registered');
        
        req.company = result.data();
        next();
    } catch (ex) {
        res.status(400).send('Invalid token');
    }
}