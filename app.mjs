import 'express-async-errors';
import express from 'express';
// import morgan from 'morgan';
import winston from './startup/winston.mjs';
import swaggerUi from 'swagger-ui-express';
import swaggerDoc from './swagger/index.mjs';
const app = express();

// app.use(morgan('combined', { stream: winston.stream }));
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDoc));

app.get('/', (req, res) => {
    res.send('Working');
});

import config from './startup/config.mjs';
config();

import routes from './startup/routes.mjs';
routes(app);

const PORT = process.env.PORT || 4000;
app.listen(PORT, () => console.log(`Listening to port ${PORT}...`));